﻿namespace Airlines.Business.Data.Aircraft;
public abstract class Aircraft(string model)
{
    public string Model { get; set; } = model;
}