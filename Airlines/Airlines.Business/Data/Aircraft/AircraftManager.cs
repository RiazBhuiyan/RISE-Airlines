﻿namespace Airlines.Business.Data.Aircraft
{
    public class AircraftManager
    {
        private readonly List<CargoAircraft> _cargoAircraftList = [];
        private readonly List<PassengerAircraft> _passengerAircraftList = [];
        private readonly List<PrivateAircraft> _privateAircraftList = [];

        public IReadOnlyList<CargoAircraft> CargoAircraftList => _cargoAircraftList;
        public IReadOnlyList<PassengerAircraft> PassengerAircraftList => _passengerAircraftList;
        public IReadOnlyList<PrivateAircraft> PrivateAircraftList => _privateAircraftList;


        public void AddAircraft(string model, string cargoWeight, string cargoVolume, string seats)
        {
            if (cargoWeight != "-" && cargoVolume != "-" && seats != "-")
            {
                var passengerAircraft = new PassengerAircraft(model, int.Parse(cargoWeight), double.Parse(cargoVolume), int.Parse(seats));
                _passengerAircraftList.Add(passengerAircraft);
            }
            else if (cargoWeight != "-" && cargoVolume != "-" && seats == "-")
            {
                var cargoAircraft = new CargoAircraft(model, int.Parse(cargoWeight), double.Parse(cargoVolume));
                _cargoAircraftList.Add(cargoAircraft);
            }
            else if (cargoWeight == "-" && cargoVolume == "-" && seats != "-")
            {
                var privateAircraft = new PrivateAircraft(model, int.Parse(seats));
                _privateAircraftList.Add(privateAircraft);
            }
            else
            {
                Console.WriteLine("Invalid input for aircraft.");
            }
        }
    }
}

