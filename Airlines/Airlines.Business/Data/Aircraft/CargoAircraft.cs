﻿namespace Airlines.Business.Data.Aircraft;
public class CargoAircraft(string model, int cargoWeight, double cargoVolume) : Aircraft(model)
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}

