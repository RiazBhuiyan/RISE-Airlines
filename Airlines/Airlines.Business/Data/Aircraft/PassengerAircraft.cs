﻿namespace Airlines.Business.Data.Aircraft;
public class PassengerAircraft(string model, int cargoWeight, double cargoVolume, int seats) : Aircraft(model)
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
    public int Seats { get; set; } = seats;
}

