﻿namespace Airlines.Business.Data.Aircraft;
public class PrivateAircraft(string model, int seats) : Aircraft(model)
{
    public int Seats { get; set; } = seats;
}

