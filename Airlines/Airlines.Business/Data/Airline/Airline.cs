﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Airline
{
    public class Airline
    {
#pragma warning disable IDE1006 
        private const int AIRLINE_NAME_MAX_LENGTH = 5;

        private string? _name;

        public Airline(string name) => Name = name;

        public string Name
        {
            get => _name ?? "";

            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new InvalidAirlineException("The airline name can't be empty.");
                }

                if (!IsValidAirline(value))
                {
                    throw new InvalidAirlineException($"The airline name can't be longer than {AIRLINE_NAME_MAX_LENGTH} characters.");
                }

                _name = value;

            }
        }

        public static bool IsValidAirline(string input) => input.Length <= AIRLINE_NAME_MAX_LENGTH;
    }
}
