﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Airline;
public class AirlineManager
{
    public AirlineManager() => AirlineDictionary = [];

    public Dictionary<string, Airline> AirlineDictionary { get; }

    public bool AirlineExists(string airlineName) => AirlineDictionary.ContainsKey(airlineName);

    public void AddAirline(Airline airline)
    {
        if (string.IsNullOrWhiteSpace(airline.Name))
        {
            throw new InvalidAirlineException("The airline name cannot be empty or whitespace.");
        }

        if (AirlineExists(airline.Name))
        {
            throw new AlreadyExistsException("The airline is already in the database.");
        }

        AirlineDictionary.Add(airline.Name, airline);
        Console.WriteLine($"Airline {airline.Name} added successfully.");
    }
}
