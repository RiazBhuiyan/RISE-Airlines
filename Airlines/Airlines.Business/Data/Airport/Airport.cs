﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;

namespace Airlines.Business.Data.Airport;
public class Airport
{
#pragma warning disable IDE1006 
    private const int AIRPORT_IDENTIFIER_LENGTH = 3;

    private string? _identifier;
    private string? _name;
    private string? _city;
    private string? _country;

    public Airport(string identifier, string name, string city, string country)
    {
        Identifier = identifier;
        Name = name;
        City = city;
        Country = country;
    }

    public string Identifier
    {
        get => _identifier ?? "";
        set
        {
            if (ValidateIdentifier(value))
            {
                _identifier = value;
            }
        }
    }

    public string Name
    {
        get => _name ?? "";
        set
        {
            if (string.IsNullOrWhiteSpace(value) || !value.All(c => char.IsLetter(c) || char.IsWhiteSpace(c)))
            {
                throw new InvalidAirportException("The airport name must have only letters or spaces.");
            }

            _name = value;
        }
    }

    public string City
    {
        get => _city ?? "";
        set
        {
            if (string.IsNullOrWhiteSpace(value) || !value.All(c => char.IsLetter(c) || char.IsWhiteSpace(c)))
            {
                throw new InvalidAirportException("The airport city must have only letters or spaces.");
            }

            _city = value;
        }
    }

    public string Country
    {
        get => _country ?? "";
        set
        {
            if (string.IsNullOrWhiteSpace(value) || !value.All(c => char.IsLetter(c) || char.IsWhiteSpace(c)))
            {
                throw new InvalidAirportException("The airport country must have only letters or spaces.");
            }

            _country = value;

        }
    }

    public static bool IsValidIdentifier(string value) => value.Length == AIRPORT_IDENTIFIER_LENGTH && DataValidator.IsAlphanumeric(value);


    // Separate method to validate the identifier
    public static bool ValidateIdentifier(string value)
    {
        try
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length != AIRPORT_IDENTIFIER_LENGTH || !DataValidator.IsAlphanumeric(value))
            {
                throw new InvalidAirportException($"The airport identifier must have {AIRPORT_IDENTIFIER_LENGTH} alphanumeric characters.");
            }

            return true;
        }
        catch (InvalidAirportException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
    }
}

