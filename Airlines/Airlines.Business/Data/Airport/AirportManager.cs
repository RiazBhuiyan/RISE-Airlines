﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Airport
{
    public class AirportManager
    {
        public AirportManager()
        {
            AirportList = [];
            AirportsByCity = [];
            AirportsByCountry = [];
        }

        public List<Airport> AirportList { get; }
        public Dictionary<string, List<Airport>> AirportsByCity { get; }
        public Dictionary<string, List<Airport>> AirportsByCountry { get; }

        public void AddAirport(Airport airport)
        {
            if (string.IsNullOrWhiteSpace(airport.Name) ||
                string.IsNullOrWhiteSpace(airport.City) ||
                string.IsNullOrWhiteSpace(airport.Country) ||
                string.IsNullOrWhiteSpace(airport.Identifier))
            {
                Console.WriteLine("Airport wasn't added.");
                return;
            }

            // Check if the airport's city is empty
            if (string.IsNullOrWhiteSpace(airport.City))
            {
                Console.WriteLine("Airport wasn't added.");
                return;
            }

            // Check if the airport's country is empty
            if (string.IsNullOrWhiteSpace(airport.Country))
            {
                Console.WriteLine("Airport wasn't added.");
                return;
            }

            // Check if the airport's identifier is empty
            if (string.IsNullOrWhiteSpace(airport.Identifier))
            {
                Console.WriteLine("Airport wasn't added.");
                return;
            }

            // Add to airport to airport list
            if (AirportList.Any(el => el.Identifier == airport.Identifier))
            {
                throw new AlreadyExistsException("The airport is already in the database.");
            }

            AirportList.Add(airport);

            // Add to airports by city dictionary
            if (!AirportsByCity.TryGetValue(airport.City, out var cityAirports))
            {
                cityAirports = [];
                AirportsByCity[airport.City] = cityAirports;
            }

            cityAirports.Add(airport);

            // Add to airports by country dictionary
            if (!AirportsByCountry.TryGetValue(airport.Country, out var countryAirports))
            {
                countryAirports = [];
                AirportsByCountry[airport.Country] = countryAirports;
            }

            countryAirports.Add(airport);

            Console.WriteLine($"Airport {airport.Name} added successfully.");
        }

        public void ListAirports(string input, string from)
        {
            if (from.Equals("city", StringComparison.OrdinalIgnoreCase))
            {
                ListAirportsByCity(input);
            }
            else if (from.Equals("country", StringComparison.OrdinalIgnoreCase))
            {
                ListAirportsByCountry(input);
            }
            else
            {
                Console.WriteLine("Invalid input for 'from'. Please specify 'city' or 'country'.");
            }
        }

        private void ListAirportsByCity(string city)
        {
            if (AirportsByCity.TryGetValue(city, out var airports))
            {
                Console.WriteLine($"Airports in {city}:");
                foreach (var airport in airports)
                {
                    Console.WriteLine($"Name: {airport.Name}, Identifier: {airport.Identifier}");
                }
            }
            else
            {
                Console.WriteLine($"No airports found in {city}.");
            }
        }

        private void ListAirportsByCountry(string country)
        {
            if (AirportsByCountry.TryGetValue(country, out var airports))
            {
                Console.WriteLine($"Airports in {country}:");
                foreach (var airport in airports)
                {
                    Console.WriteLine($"Name: {airport.Name}, Identifier: {airport.Identifier}");
                }
            }
            else
            {
                Console.WriteLine($"No airports found in {country}.");
            }
        }
    }
}
