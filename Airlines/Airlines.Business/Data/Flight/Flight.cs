﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;

namespace Airlines.Business.Data.Flight;
public class Flight
{
    private string? _identifier;
    private string? _departureAirport;
    private string? _arrivalAirport;
    private string? _aircraftModel;

    // All setters have a Null check, the warning appears to be wrong.
    public Flight(string identifier, string departureAirport, string arrivalAirport, string aircraftModel)
    {
        Identifier = identifier;
        DepartureAirport = departureAirport;
        ArrivalAirport = arrivalAirport;
        AircraftModel = aircraftModel;
    }

    public string Identifier
    {
        get => _identifier ?? "";

        set
        {
            if (string.IsNullOrWhiteSpace(value) || !DataValidator.IsAlphanumeric(value))
            {
                throw new InvalidFlightCodeException($"The flight identifier is mandatory and it must have only alphanumeric characters.");
            }

            _identifier = value;
        }
    }

    public string DepartureAirport
    {
        get => _departureAirport ?? "";

        set
        {
            if (Airport.Airport.ValidateIdentifier(value))
            {
                _departureAirport = value;
            }
        }
    }

    public string ArrivalAirport
    {
        get => _arrivalAirport ?? "";

        set
        {
            if (Airport.Airport.ValidateIdentifier(value))
            {
                _arrivalAirport = value;
            }
        }
    }

    public string AircraftModel
    {
        get => _aircraftModel ?? "";

        set
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new InvalidAircraftParameterException($"The aircraft model is mandatory.");
            }

            _aircraftModel = value;
        }
    }
}

