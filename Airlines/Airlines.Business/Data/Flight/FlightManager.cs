﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Flight
{
    public class FlightManager
    {
        public FlightManager()
        {
            FlightList = [];
            RouteTree = new FlightRouteTree("");
        }

        public List<Flight> FlightList { get; }

        public Route? Route { get; set; }
        public FlightRouteTree RouteTree { get; set; }

        public Flight? GetFlightByIdentifier(string identifier)
            => FlightList.FirstOrDefault(f => f.Identifier == identifier);

        public void AddFlight(Flight flight)
        {
            if (FlightList.Any(el => el.Identifier == flight.Identifier))
            {
                throw new AlreadyExistsException("The flight is already in the database.");
            }

            FlightList.Add(flight);
            Console.WriteLine($"{flight.Identifier} added successfully.");
        }
        public void LoadRouteTreeData(List<string[]> routeData)
        {
            if (routeData == null || routeData.Count == 0)
            {
                throw new InvalidRouteException("Route data cannot be null or empty.");
            }

            var startAirport = new FlightRouteTree(routeData[0][0]);
            RouteTree = startAirport;


            for (var i = 1; i < routeData.Count; i++)
            {
                var flightIdentifier = routeData[i][0];
                var currentFlight = GetFlightByIdentifier(flightIdentifier) ?? throw new InvalidFlightCodeException($"Flight '{flightIdentifier}' does not exist in the database.");

                if (currentFlight.DepartureAirport == startAirport.Root)
                {
                    startAirport.AddChild(currentFlight);
                }
                else
                {
                    var departureAirportNode = startAirport.FindRoute(currentFlight.DepartureAirport);

                    if (departureAirportNode != null)
                    {
                        if (departureAirportNode.Children.Any(node => node.Value == currentFlight))
                        {
                            throw new InvalidRouteException($"Flight '{currentFlight.Identifier}' is already in the route.");
                        }

                        departureAirportNode.AddChild(currentFlight);
                    }
                }
            }
        }


    }
}