﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Flight;
public class FlightRouteTree
{
    private string _root;

#pragma warning disable CS8618
    public FlightRouteTree(string root) => Root = root;

    public string Root
    {
        get => _root ?? "";
        set
        {
            _ = Airport.Airport.ValidateIdentifier(value);
            _root = value;
        }
    }

    public List<FlightRouteTreeNode> Children { get; } = [];

    public void AddChild(Flight childFlight)
    {
        if (childFlight.DepartureAirport == childFlight.ArrivalAirport)
        {
            throw new InvalidRouteException("Flight route has the same airport for both arrival and departure.");
        }

        Children.Add(new FlightRouteTreeNode(childFlight));
    }

    public FlightRouteTreeNode? FindRoute(string arrivalAirport)
    {
        foreach (var child in Children)
        {
            var result = child.FindRoute(arrivalAirport);

            if (result != null)
            {
                return result;
            }
        }

        return null;
    }

    public string? GetFlightRoute(string arrivalAirport)
    {
        var route = new List<string>();
        _ = GetFlightRouteRecursive(arrivalAirport, route);

        if (route.Count == 0)
        {
            return null;
        }

        return string.Join("->", route);
    }

    private bool GetFlightRouteRecursive(string arrivalAirport, List<string> route)
    {
        route.Add(_root);
        foreach (var child in Children)
        {
            if (child.SearchRouteRecursive(_root, arrivalAirport, route))
            {
                return true;
            }
        }

        route.RemoveAt(route.Count - 1);

        return false;
    }
}
