﻿using Airlines.Business.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Business.Data.Flight;
public class FlightRouteTreeNode(Flight value)
{
    public Flight Value { get; private set; } = value ?? throw new InvalidInputException("Cannot insert a null value.");

    public List<FlightRouteTreeNode> Children { get; } = [];

    public void AddChild(Flight childFlight)
    {
        if (childFlight == null)
        {
            throw new InvalidInputException("Cannot add a null flight as a child.");
        }

        if (childFlight.DepartureAirport == childFlight.ArrivalAirport)
        {
            throw new InvalidRouteException("Flight route has the same airport for both arrival and departure.");
        }

        Children.Add(new FlightRouteTreeNode(childFlight));
    }

    public FlightRouteTreeNode? FindRoute(string arrivalAirport)
    {
        if (Value.ArrivalAirport == arrivalAirport)
        {
            return this;
        }

        foreach (var child in Children)
        {
            var result = child.FindRoute(arrivalAirport);
            if (result != null)
            {
                return result;
            }
        }

        return null;
    }

    public bool SearchRouteRecursive(string currentAirport, string arrivalAirport, List<string> route)
    {
        if (Value.DepartureAirport == currentAirport)
        {
            route.Add(Value.ArrivalAirport);

            if (Value.ArrivalAirport == arrivalAirport)
            {
                return true;
            }

            foreach (var child in Children)
            {
                if (child.SearchRouteRecursive(Value.ArrivalAirport, arrivalAirport, route))
                {
                    return true;
                }
            }

            route.RemoveAt(route.Count - 1);
        }

        return false;
    }
}