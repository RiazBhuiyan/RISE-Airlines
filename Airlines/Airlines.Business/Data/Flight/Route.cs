﻿using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Flight;
public class Route
{
    private readonly LinkedList<Flight> _route;

    public Route() => _route = new LinkedList<Flight>();

    public IEnumerable<Flight> RouteList => _route;

    public void AddFlight(string flightIdentifier, FlightManager flightManager)
    {
        var flight = flightManager.GetFlightByIdentifier(flightIdentifier) ?? throw new InvalidFlightCodeException($"{flightIdentifier} not found. Add the flight before you add it to a route!");
        if (_route.Count > 0 && _route.Last!.Value.ArrivalAirport != flight.DepartureAirport)
        {
            throw new InvalidRouteException("New flight does not connect logically to the route.");
        }

        _ = _route.AddLast(flight);
        Console.WriteLine($"Flight {flight.Identifier} added to the route.");
    }

    public void RemoveLastFlight()
    {
        if (_route.Count == 0)
        {
            throw new InvalidRouteException("Error: Route is empty.");
        }

        var removedFlight = _route.Last!.Value;
        _route.RemoveLast();
        Console.WriteLine($"Flight {removedFlight.Identifier} removed from the route.");
    }
}

