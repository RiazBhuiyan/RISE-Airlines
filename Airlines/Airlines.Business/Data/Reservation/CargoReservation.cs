﻿namespace Airlines.Business.Data.Reservation;
public class CargoReservation(string flightIdentifier, int cargoWeight, double cargoVolume) : Reservation(flightIdentifier)
{
    public int CargoWeight { get; set; } = cargoWeight;
    public double CargoVolume { get; set; } = cargoVolume;
}

