﻿namespace Airlines.Business.Data.Reservation;
public abstract class Reservation(string flightIdentifier)
{
    public string FlightIdentifier { get; set; } = flightIdentifier;
}

