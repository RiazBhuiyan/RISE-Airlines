﻿using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;

namespace Airlines.Business.Data.Reservation;
public class ReservationManager
{

    private readonly List<CargoReservation> _cargoReservationList = [];
    private readonly List<TicketReservation> _ticketReservationList = [];

    public IReadOnlyList<CargoReservation> CargoReservationList => _cargoReservationList;
    public IReadOnlyList<TicketReservation> TicketReservationList => _ticketReservationList;

#pragma warning disable IDE1006 // Naming Styles
    private const int SMALL_BAGGAGE_WEIGHT = 15;
    private const int LARGE_BAGGAGE_WEIGHT = 30;
    private const double SMALL_BAGGAGE_VOLUME = 0.045;
    private const double LARGE_BAGGAGE_VOLUME = 0.090;

    public static bool isValidCargo(int aircraftCargoWeight, double aircraftCargoVolume, int cargoWeight, double cargoVolume)
    {
        try
        {
            if (aircraftCargoWeight < cargoWeight)
            {
                throw new InvalidReservationParameterException("The cargo exceeds the aircraft weight limit.");
            }

            if (aircraftCargoVolume < cargoVolume)
            {
                throw new InvalidReservationParameterException("The cargo volume exceeds the aircraft volume limit.");
            }

            return true;
        }
        catch (InvalidReservationParameterException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
    }

    public static bool isValidSeats(int aircraftSeats, int seats)
    {
        try
        {
            if (aircraftSeats < seats)
            {
                throw new InvalidReservationParameterException($"Not enough seats. {aircraftSeats} are left.");
            }

            return true;
        }
        catch (InvalidReservationParameterException ex)
        {
            Console.WriteLine(ex.Message);
            return false;
        }
    }

    public static Flight.Flight GetFlightByIdentifier(FlightManager flightManager, string flightIdentifier)
    {
        var flight = flightManager.GetFlightByIdentifier(flightIdentifier)
            ?? throw new InvalidFlightCodeException($"Flight with identifier {flightIdentifier} doesn't exist.");

        return flight;
    }

    public void ReserveCargo(
        string flightIdentifier,
        int cargoWeight,
        double cargoVolume,
        FlightManager flightManager,
        AircraftManager aircraftManager
        )
    {
        var flight = GetFlightByIdentifier(flightManager, flightIdentifier);

        var aircraft = aircraftManager.CargoAircraftList.FirstOrDefault(aircraft => aircraft.Model == flight.AircraftModel) ?? throw new InvalidAircraftParameterException($"Cargo aircraft with model {flight.AircraftModel} doesn't exist.");
        if (isValidCargo(aircraft.CargoWeight, aircraft.CargoVolume, cargoWeight, cargoVolume))
        {
            aircraft.CargoWeight -= cargoWeight;
            aircraft.CargoVolume -= cargoVolume;

            var reservation = new CargoReservation(flightIdentifier, cargoWeight, cargoVolume);
            _cargoReservationList.Add(reservation);
            Console.WriteLine($"Cargo reservation for {flightIdentifier} submitted.");
        }
    }

    public void ReserveTicket(
        string flightIdentifier,
        int seats,
        int smallBaggageCount,
        int largeBaggageCount,
        FlightManager flightManager,
        AircraftManager aircraftManager
        )
    {
        var flight = GetFlightByIdentifier(flightManager, flightIdentifier);

        var aircraft = aircraftManager.PassengerAircraftList.FirstOrDefault(aircraft => aircraft.Model == flight.AircraftModel) ?? throw new InvalidAircraftParameterException($"Passenger aircraft with model {flight.AircraftModel} doesn't exist.");
        var cargoWeight = (smallBaggageCount * SMALL_BAGGAGE_WEIGHT) + (largeBaggageCount * LARGE_BAGGAGE_WEIGHT);

        var cargoVolume = (smallBaggageCount * SMALL_BAGGAGE_VOLUME) + (largeBaggageCount * LARGE_BAGGAGE_VOLUME);

        if (isValidCargo(aircraft.CargoWeight, aircraft.CargoVolume, cargoWeight, cargoVolume) && isValidSeats(aircraft.Seats, seats))
        {
            aircraft.Seats -= seats;
            aircraft.CargoWeight -= cargoWeight;
            aircraft.CargoVolume -= cargoVolume;

            var reservation = new TicketReservation(flightIdentifier, seats, smallBaggageCount, largeBaggageCount);
            _ticketReservationList.Add(reservation);
            Console.WriteLine($"Passenger reservation for {flightIdentifier} submitted.");
        }
    }
}

