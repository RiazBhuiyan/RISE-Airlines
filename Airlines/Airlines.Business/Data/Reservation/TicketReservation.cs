﻿namespace Airlines.Business.Data.Reservation;
public class TicketReservation(string flightIdentifier, int seats, int smallBaggageCount, int largeBaggageCount) : Reservation(flightIdentifier)
{
    public int Seats { get; set; } = seats;
    public int SmallBaggageCount { get; set; } = smallBaggageCount;
    public int LargeBaggageCount { get; set; } = largeBaggageCount;
}

