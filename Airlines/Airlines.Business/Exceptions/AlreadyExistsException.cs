﻿namespace Airlines.Business.Exceptions;
public class AlreadyExistsException(string message) : Exception(message)
{
}
