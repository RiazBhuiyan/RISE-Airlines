﻿namespace Airlines.Business.Exceptions;
public class InvalidAirlineException(string message) : Exception(message)
{
}