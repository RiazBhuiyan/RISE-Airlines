﻿namespace Airlines.Business.Exceptions;
public class InvalidAirportException(string message) : Exception(message)
{
}
