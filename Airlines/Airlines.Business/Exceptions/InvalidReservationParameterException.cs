﻿namespace Airlines.Business.Exceptions;
public class InvalidReservationParameterException(string message) : Exception(message)
{
}
