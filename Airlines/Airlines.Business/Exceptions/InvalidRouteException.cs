﻿
namespace Airlines.Business.Exceptions;
public class InvalidRouteException(string message) : Exception(message)
{
}
