﻿namespace Airlines.Business.Utilities;
public static class DataValidator
{
    // Method to check if a string consists only of alphabetic characters
    public static bool IsAlphabetic(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetter(c))
            {
                return false;
            }
        }
        return true;
    }

    // Method to check if a string consists only of alphabetic characters and spaces
    public static bool IsAlphabeticAndSpaces(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetter(c) && c != ' ')
            {
                return false;
            }
        }
        return true;
    }

    // Method to check if a string consists only of alphanumeric characters
    public static bool IsAlphanumeric(string input)
    {
        foreach (var c in input)
        {
            if (!char.IsLetterOrDigit(c))
            {
                return false;
            }
        }
        return true;
    }

    // Method to validate input based on type
    public static bool ValidateInput(string input, string type)
    {
        // Perform validation based on the input type
        return type.ToLower() switch
        {
            "airports" => input.Length == 3 && IsAlphabetic(input),
            "airlines" => input.Length < 6,
            "flights" => IsAlphanumeric(input),
            _ => false,
        };
    }
}
