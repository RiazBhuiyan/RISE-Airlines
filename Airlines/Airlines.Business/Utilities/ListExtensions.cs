﻿using System;
using System.Collections.Generic;

namespace Airlines.Business.Utilities
{
    public static class ListExtensions
    {
        public static void BubbleSort(this List<string> list, string sortingOrder = "ascending")
        {
            if (!string.Equals(sortingOrder.ToLower(), "ascending") && !string.Equals(sortingOrder.ToLower(), "descending"))
            {
                Console.WriteLine("Invalid sort order. Choose between 'ascending' and 'descending'.");
                return;
            }

            var n = list.Count;
            for (var i = 0; i < n - 1; i++)
            {
                for (var j = 0; j < n - i - 1; j++)
                {
                    var comparison = sortingOrder.Equals("ascending", StringComparison.CurrentCultureIgnoreCase) ? string.Compare(list[j], list[j + 1]) : string.Compare(list[j + 1], list[j]);
                    if (comparison > 0)
                    {
                        list.SwapItems(j, j + 1);
                    }
                }
            }
        }


        public static void SelectionSort(this List<string> list, string sortingOrder = "ascending")
        {
            if (!string.Equals(sortingOrder.ToLower(), "ascending") && !string.Equals(sortingOrder.ToLower(), "descending"))
            {
                Console.WriteLine("Invalid sort order. Choose between 'ascending' and 'descending'.");
                return;
            }

            var n = list.Count;

            for (var i = 0; i < n - 1; i++)
            {
                // Find the index of the minimum (or maximum) element in the unsorted portion of the list
                var sortIndex = i;
                for (var j = i + 1; j < n; j++)
                {
                    var comparison = sortingOrder.Equals("ascending", StringComparison.CurrentCultureIgnoreCase) ? string.Compare(list[j], list[sortIndex]) : string.Compare(list[sortIndex], list[j]);
                    if (comparison < 0)
                    {
                        sortIndex = j;
                    }
                }

                // Swap the found minimum (or maximum) element with the first element of the unsorted portion
                if (sortIndex != i)
                {
                    list.SwapItems(i, sortIndex);
                }
            }
        }


        public static bool BinarySearch(this List<string> list, string target)
        {
            var left = 0;
            var right = list.Count - 1;

            while (left <= right)
            {
                var mid = left + ((right - left) / 2);

                var comparisonResult = string.Compare(list[mid], target);

                if (comparisonResult == 0)
                {
                    return true;
                }
                else if (comparisonResult < 0)
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid - 1;
                }
            }

            return false;
        }

        public static bool ValidateUniqueness(this List<string> list)
        {
            for (var i = 0; i < list.Count; i++)
            {
                for (var j = i + 1; j < list.Count; j++)
                {
                    if (list[i] == list[j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static string ListToString(List<string> list)
        {
            var listString = string.Join(", ", list);
            return listString;
        }

        public static void SwapItems(this List<string> list, int indexItemOne, int indexItemTwo) => (list[indexItemTwo], list[indexItemOne]) = (list[indexItemOne], list[indexItemTwo]);
    }
}
