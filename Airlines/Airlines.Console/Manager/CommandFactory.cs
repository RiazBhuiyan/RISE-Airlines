﻿using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Data.Reservation;
using Airlines.Console.Manager.Commands;

namespace Airlines.Console.Manager;
public static class CommandFactory
{
    public static ICommand CreateCommand(CommandParser.CommandType commandType,
        string[] parameters,
        AirportManager airportManager,
        AirlineManager airlineManager,
        FlightManager flightManager,
        AircraftManager aircraftManager,
        ReservationManager reservationManager)
    {
        var airports = airportManager.AirportList.Select(el => el.Identifier).ToList();
        var airlines = airlineManager.AirlineDictionary.Values.Select(el => el.Name).ToList();
        var flights = flightManager.FlightList.Select(el => el.Identifier).ToList();

#pragma warning disable CS8603 // Possible null reference return.
#pragma warning disable IDE0072 // Add missing cases
        return commandType switch
        {
            CommandParser.CommandType.Add => AddCommand.Create(parameters, airportManager, airlineManager, flightManager),
            CommandParser.CommandType.Sort => SortCommand.Create(parameters, airports, airlines, flights),
            CommandParser.CommandType.Print => PrintListsCommand.Create(airports, airlines, flights),
            CommandParser.CommandType.Search => SearchCommand.Create(parameters, airportManager, airlineManager, flightManager),
            CommandParser.CommandType.Exist => AirlineExistCommand.Create(parameters, airlineManager),
            CommandParser.CommandType.List => ListCommand.Create(parameters, airportManager),
            CommandParser.CommandType.Route => RouteCommand.Create(parameters, flightManager),
            CommandParser.CommandType.Reserve => ReserveCommand.Create(parameters, flightManager, aircraftManager, reservationManager),
            CommandParser.CommandType.Help => HelpCommand.Create(),
            _ => throw new NotSupportedException("Unsupported command."),
        };
    }
}

