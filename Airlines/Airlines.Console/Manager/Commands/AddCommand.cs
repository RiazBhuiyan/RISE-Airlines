﻿using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;

namespace Airlines.Console.Manager.Commands;
public class AddCommand : ICommand
{
    private enum CollectionType
    {
        Airport,
        Airline,
        Flight
    }

    private readonly string[] _parameters;
    private readonly AirportManager _airportManager;
    private readonly AirlineManager _airlineManager;
    private readonly FlightManager _flightManager;

    private AddCommand(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        _parameters = parameters;
        _airportManager = airportManager;
        _airlineManager = airlineManager;
        _flightManager = flightManager;
    }
    private void HandleAirport()
    {
        var airportParams = string.Join(" ", _parameters.Skip(1)).Split(',');
        var airport = new Airport(airportParams[0], airportParams[1], airportParams[2], airportParams[3]);
        _airportManager.AddAirport(airport);
    }

    private void HandleAirline()
    {
        var airlineName = string.Join(" ", _parameters.Skip(1));
        var airline = new Airline(airlineName);
        _airlineManager.AddAirline(airline);
    }

    private void HandleFlight()
    {
        var flightParams = string.Join(" ", _parameters.Skip(1)).Split(',');
        var flight = new Flight(flightParams[0], flightParams[1], flightParams[2], flightParams[3]);
        _flightManager.AddFlight(flight);
    }

    public void Execute()
    {
        if (_parameters.Length < 2)
        {
            System.Console.WriteLine("Invalid list type or command parameters for the 'add' command. Valid format add <list> <entity name>.");
            return;
        }

        var type = Enum.Parse<CollectionType>(_parameters[0], true);
        switch (type)
        {
            case CollectionType.Airport:
                HandleAirport();
                break;
            case CollectionType.Airline:
                HandleAirline();
                break;
            case CollectionType.Flight:
                HandleFlight();
                break;
            default:
                throw new InvalidInputException("Invalid list type for add command.");
        }
    }

    public static AddCommand Create(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        if (parameters == null || parameters.Length < 2)
        {
            throw new InvalidInputException("Invalid parameters for 'add' command.");
        }

        if (!Enum.TryParse(parameters[0], true, out CollectionType _) || parameters.Length < 2)
        {
            throw new InvalidInputException("Invalid list type or command parameters for the 'add' command. Valid format add <list> <entity name>.");
        }

        return new AddCommand(parameters, airportManager, airlineManager, flightManager);
    }
}

