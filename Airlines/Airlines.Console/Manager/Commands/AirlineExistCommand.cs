﻿using Airlines.Business.Data.Airline;
using Airlines.Business.Exceptions;

namespace Airlines.Console.Manager.Commands;

public class AirlineExistCommand : ICommand
{
    private readonly string[] _parameters;
    private readonly AirlineManager _airlineManager;

    private AirlineExistCommand(string[] parameters, AirlineManager airlineManager)
    {
        _parameters = parameters;
        _airlineManager = airlineManager;
    }

    public void Execute()
    {
        if (_parameters == null || _parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameter for 'exist' command.");
        }
        var airlineName = string.Join(" ", _parameters);

        System.Console.WriteLine(_airlineManager.AirlineExists(airlineName).ToString());
    }

    public static AirlineExistCommand Create(string[] parameters, AirlineManager airlineManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameter for 'exist' command.");
        }

        return new AirlineExistCommand(parameters, airlineManager);
    }
}

