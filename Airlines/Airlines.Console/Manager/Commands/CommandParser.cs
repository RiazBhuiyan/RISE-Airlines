﻿using Airlines.Business.Exceptions;

namespace Airlines.Console.Manager.Commands;
public static class CommandParser
{
    public enum CommandType
    {
        Add,
        Sort,
        Print,
        Search,
        Exist,
        List,
        Route,
        Reserve,
        Batch,
        Help,
        Exit
    }

    public static (CommandType command, string[] parameters) Parse(string input)
    {
        var splitInput = input.Split(" ");
        _ = Enum.TryParse(splitInput[0], true, out CommandType command) ?
            command :
            throw new InvalidInputException("Invalid command.");

        var parameters = splitInput.Skip(1).ToArray() ?? [];
        return (command, parameters);
    }
}
