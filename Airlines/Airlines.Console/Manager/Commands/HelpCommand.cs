﻿namespace Airlines.Console.Manager.Commands;
public class HelpCommand : ICommand
{
    public HelpCommand() { }
    public void Execute()
    {
        System.Console.WriteLine("Command list:");
        System.Console.WriteLine("add airport <identifier>,<name>,<city>,<country>: " + "Adds a new airport to the list");
        System.Console.WriteLine("add airline <name>: Adds a new airline to the list");
        System.Console.WriteLine("add flight  <name>: Adds a new flight to the list");
        System.Console.WriteLine("sort <list> <optional: sort order>: Sorts a list. Default is in ascending order!");
        System.Console.WriteLine("search <search term>: Search for a term in all lists. Sort all lists before searching!");
        System.Console.WriteLine("print: Print Airports, Airlines, and Flights");
        System.Console.WriteLine("exist <airline name>: Returns if an airline exists");
        System.Console.WriteLine("list <name of city/country>,<type of city/country>: Displays all airports in a city or country");
        System.Console.WriteLine("route new: Creates a new route.");
        System.Console.WriteLine("route add <existing flight identifier>: Adds a new route for an existing flight that logically connects to the route.");
        System.Console.WriteLine("route remove: Removes the last route.");
        System.Console.WriteLine("route find <Destination Airport>: Search for a flight route to the specified destination airport.");
        System.Console.WriteLine("route print: Prints the routes.");
        System.Console.WriteLine("reserve cargo <flight identifier>,<cargo weight>,<cargo volume>: Reserves cargo for the specified existing flight.");
        System.Console.WriteLine("reserve ticket <flight identifier>,<seats>,<small baggage count>,<large baggage count>: Reserves a ticket for the specified existing flight.");
        System.Console.WriteLine("batch start: Batch mode ON.");
        System.Console.WriteLine("batch execute: Runs commands from the batch queue.");
        System.Console.WriteLine("batch cancel: Cancels batch mode and clears the batch queue.");
        System.Console.WriteLine("help: Display available commands");
        System.Console.WriteLine("exit: Exit the program");
    }

    public static HelpCommand Create() => new();
}

