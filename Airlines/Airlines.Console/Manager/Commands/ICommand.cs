﻿namespace Airlines.Console.Manager.Commands;
public interface ICommand
{
    void Execute();
}
