﻿using Airlines.Business.Data.Airport;
using Airlines.Business.Exceptions;

namespace Airlines.Console.Manager.Commands;
public class ListCommand : ICommand
{
    private enum AirportListType
    {
        City,
        Country
    }

    private readonly string[] _parameters;
    private readonly AirportManager _airportManager;

    private ListCommand(string[] parameters, AirportManager airportManager)
    {
        _parameters = parameters;
        _airportManager = airportManager;
    }

    public void Execute()
    {
        if (_parameters == null || _parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameters for 'list' command. Correct format: List <name of city/country>,<type of city/country>.");
        }

        var airportParams = string.Join(" ", _parameters).Split(',');

        if (airportParams.Length != 2)
        {
            throw new InvalidInputException("Invalid parameters for 'list' command. Correct format: List <name of city/country>,<type of city/country>.");
        }

        if (Enum.TryParse(airportParams[1], true, out AirportListType listType))
        {
            _airportManager.ListAirports(airportParams[0], listType.ToString());
        }
        else
        {
            throw new InvalidInputException("Invalid parameter for city/country");
        }
    }

    public static ListCommand Create(string[] parameters, AirportManager airportManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameters for 'list' command. Correct format: List <name of city/country>,<type of city/country>.");
        }

        var airportParams = string.Join(" ", parameters).Split(',');

        if (airportParams.Length != 2)
        {
            throw new InvalidInputException("Invalid parameters for 'list' command. Correct format: List <name of city/country>,<type of city/country>.");
        }

        if (!Enum.TryParse(airportParams[1], true, out AirportListType _))
        {
            throw new InvalidInputException("Invalid parameter for city/country");
        }

        return new ListCommand(parameters, airportManager);
    }
}

