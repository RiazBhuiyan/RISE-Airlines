﻿using Airlines.Business.Utilities;

namespace Airlines.Console.Manager.Commands;
public class PrintListsCommand : ICommand
{
    private readonly List<string> _airports;
    private readonly List<string> _airlines;
    private readonly List<string> _flights;

    private PrintListsCommand(List<string> airports, List<string> airlines, List<string> flights)
    {
        _airports = airports;
        _airlines = airlines;
        _flights = flights;
    }

    public void Execute()
    {
        System.Console.WriteLine("Final Data:");

        var airportsString = ListExtensions.ListToString(_airports);
        var airlinesString = ListExtensions.ListToString(_airlines);
        var flightsString = ListExtensions.ListToString(_flights);

        System.Console.WriteLine($"Airports: {airportsString}");
        System.Console.WriteLine($"Airlines: {airlinesString}");
        System.Console.WriteLine($"Flights: {flightsString}");
    }
    public static PrintListsCommand Create(List<string> airports, List<string> airlines, List<string> flights) => new(airports, airlines, flights);
}