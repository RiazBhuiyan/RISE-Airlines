﻿using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;

namespace Airlines.Console.Manager.Commands;
public class RouteCommand : ICommand
{
    private enum RouteCommands
    {
        New,
        Add,
        Remove,
        Print,
        Find
    }

    private readonly string[] _parameters;
    private readonly FlightManager _flightManager;

    private RouteCommand(string[] parameters, FlightManager flightManager)
    {
        _parameters = parameters;
        _flightManager = flightManager;
    }

    public void Execute()
    {
        if (
            _parameters != null &&
            Enum.TryParse(_parameters[0], true, out RouteCommands routeCommand) &&
            _parameters.Length >= 1
            )
        {
            switch (routeCommand)
            {
                case RouteCommands.New:
                    _flightManager.Route = new Route();
                    break;
                case RouteCommands.Add:
                    if (_flightManager.Route != null)
                    {
                        _flightManager.Route.AddFlight(_parameters[1], _flightManager);
                    }
                    else
                    {
                        System.Console.WriteLine("Route not initialized.");
                    }
                    break;
                case RouteCommands.Remove:
                    if (_flightManager.Route != null)
                    {
                        _flightManager.Route.RemoveLastFlight();
                    }
                    else
                    {
                        System.Console.WriteLine("Route not initialized.");
                    }
                    break;
                case RouteCommands.Print:
                    if (_flightManager.Route != null)
                    {
                        PrintRoutes(_flightManager);
                    }
                    else
                    {
                        System.Console.WriteLine("Route not initialized.");
                    }
                    break;
                case RouteCommands.Find:
                    if (_flightManager.Route != null)
                    {
                        System.Console.WriteLine(_flightManager.RouteTree.GetFlightRoute(_parameters[1]));
                    }
                    else
                    {
                        System.Console.WriteLine("Route not initialized.");
                    }
                    break;
                default:
                    System.Console.WriteLine("Invalid route command.");
                    break;
            }
        }
        else
        {
            System.Console.WriteLine("Invalid route command.");
        }
    }

    private static void PrintRoutes(FlightManager flightManager)
    {
        if (flightManager == null || flightManager.Route == null || !flightManager.Route.RouteList.Any())
        {
            System.Console.WriteLine("Route empty.");
            return;
        }

        System.Console.WriteLine("Route Details:");
        foreach (var flight in flightManager.Route.RouteList)
        {
            System.Console.WriteLine($"Flight {flight.Identifier}, Departure Airport: {flight.DepartureAirport}, Arrival Airport: {flight.ArrivalAirport}.");
        }
    }

    public static RouteCommand Create(string[] parameters, FlightManager flightManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid parameters for 'route' command.");
        }

        if (!Enum.TryParse(parameters[0], true, out RouteCommands _))
        {
            throw new InvalidInputException("Invalid route command.");
        }

        return new RouteCommand(parameters, flightManager);
    }
}

