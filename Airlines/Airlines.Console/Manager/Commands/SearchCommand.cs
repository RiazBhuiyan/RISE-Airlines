﻿using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;


namespace Airlines.Console.Manager.Commands;
public class SearchCommand : ICommand
{
    private readonly string[] _parameters;
    private readonly AirportManager _airportManager;
    private readonly AirlineManager _airlineManager;
    private readonly FlightManager _flightManager;

    // Method to handle search functionality
    private SearchCommand
        (
        string[] parameters,
        AirportManager airportManager,
        AirlineManager airlineManager,
        FlightManager flightManager
        )
    {
        _parameters = parameters;
        _airportManager = airportManager;
        _airlineManager = airlineManager;
        _flightManager = flightManager;
    }

    public void Execute()
    {
        var airports = _airportManager.AirportList;
        var airlines = _airlineManager.AirlineDictionary.Values.ToList();
        var flights = _flightManager.FlightList;

        if (_parameters != null && _parameters.Length >= 1)
        {
            var searchTerm = string.Join(" ", _parameters);
            SearchAllLists(airports, airlines, flights, searchTerm);
        }
        else
            throw new InvalidInputException("Invalid search command format. Input your query after the search command.");
    }

    public static void SearchAllLists(List<Airport> airports, List<Airline> airlines, List<Flight> flights, string searchTerm)
    {
        string? result;

        if (airports.Count > 0)
        {
            result = airports.FirstOrDefault(el => el.Identifier == searchTerm)?.Identifier;

            if (!string.IsNullOrWhiteSpace(result))
            {
                System.Console.WriteLine($"Airport found: {result}.");
                return;
            }
        }

        if (airlines.Count > 0)
        {
            result = airlines.FirstOrDefault(el => el.Name == searchTerm)?.Name;

            if (!string.IsNullOrWhiteSpace(result))
            {
                System.Console.WriteLine($"Airline found: {result}.");
                return;
            }
        }

        if (flights.Count > 0)
        {
            result = flights.FirstOrDefault(el => el.Identifier == searchTerm)?.Identifier;

            if (!string.IsNullOrWhiteSpace(result))
            {
                System.Console.WriteLine($"Flight found: {searchTerm}.");
                return;
            }
        }

        System.Console.WriteLine($"{searchTerm} not found.");
    }

    public static SearchCommand? Create(string[] parameters, AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager)
    {
        if (parameters == null || parameters.Length < 1)
        {
            throw new InvalidInputException("Invalid search command format. Input your query after the search command.");
        }

        return new SearchCommand(parameters, airportManager, airlineManager, flightManager);
    }
}
