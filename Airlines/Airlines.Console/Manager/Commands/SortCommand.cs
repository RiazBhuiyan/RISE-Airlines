﻿using Airlines.Business.Exceptions;
using Airlines.Business.Utilities;

namespace Airlines.Console.Manager.Commands;
public class SortCommand(string[] parameters, List<string> airports, List<string> airlines, List<string> flights) : ICommand
{
    private enum SortTarget
    {
        Airports,
        Airlines,
        Flights
    }

    private enum SortOrder
    {
        Ascending,
        Descending
    }


    private readonly List<string> _airports = airports;
    private readonly List<string> _airlines = airlines;
    private readonly List<string> _flights = flights;
    private readonly string[] _parameters = parameters;

    // Method to handle sort command
    public void Execute()
    {
        if (_parameters == null)
        {
            System.Console.WriteLine("Invalid format of sort command.");
            return;
        }

        var sortOrder = SortOrder.Ascending;

        if (_parameters.Length == 2)
            if (!Enum.TryParse(_parameters[1], true, out sortOrder))
            {
                System.Console.WriteLine("Invalid sorting order.");
                return;
            }

        if (Enum.TryParse(_parameters[0], true, out SortTarget sortTarget))
            switch (sortTarget)
            {
                case SortTarget.Airports:
                    _airports.BubbleSort(sortOrder.ToString());
                    System.Console.WriteLine($"Airports Sorted {ListExtensions.ListToString(_airports)}");
                    break;
                case SortTarget.Airlines:
                    _airlines.SelectionSort(sortOrder.ToString());
                    System.Console.WriteLine($"Airlines Sorted {ListExtensions.ListToString(_airlines)}");
                    break;
                case SortTarget.Flights:
                    _flights.SelectionSort(sortOrder.ToString());
                    System.Console.WriteLine($"Flights Sorted {ListExtensions.ListToString(_flights)}");
                    break;
                default:
                    System.Console.WriteLine("Invalid list type for sort command.");
                    break;
            }
        else
            System.Console.WriteLine("Invalid list type for sort command.");
    }

    public static SortCommand Create(string[] parameters, List<string> airports, List<string> airlines, List<string> flights)
    {
        if (parameters == null || parameters.Length == 0)
        {
            throw new InvalidInputException("Invalid format of sort command.");
        }

        if (!Enum.TryParse(parameters[0], true, out SortTarget _))
        {
            throw new InvalidInputException("Invalid list type for sort command.");
        }

        return new SortCommand(parameters, airports, airlines, flights);
    }
}
