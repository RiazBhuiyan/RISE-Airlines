﻿using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Data.Reservation;
using Airlines.Business.Exceptions;
using Airlines.Console.Manager.Commands;
using System;

namespace Airlines.Console.Manager;
public static class ConsoleManager
{
    public static void RunProgram(AirportManager airportManager, AirlineManager airlineManager, FlightManager flightManager, AircraftManager aircraftManager,
        ReservationManager reservationManager, ExecutionManager executionManager)
    {
        HelpCommand.Create().Execute();

        while (true)
        {
            var input = System.Console.ReadLine()!.Trim();
            try
            {
                var (commandType, parameters) = CommandParser.Parse(input);

                if (commandType == CommandParser.CommandType.Exit)
                {
                    return;
                }

                if (commandType == CommandParser.CommandType.Batch)
                {
                    executionManager.ExecuteBatchCommand(parameters);
                }
                else
                {
                    var command = CommandFactory.CreateCommand(commandType,
                        parameters,
                        airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager);

                    if (executionManager.Mode == ExecutionManager.ExecutionMode.Batch)
                    {
                        executionManager.AddToBatch(command);
                    }
                    else
                    {
                        command?.Execute();
                    }
                }
            }
            catch (InvalidInputException exception)
            {
                System.Console.WriteLine($"Invalid input error: {exception.Message}");
            }
            catch (Exception exception)
            {
                System.Console.WriteLine(exception.Message);
            }
        }
    }
}