﻿using Airlines.Business.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airlines.Console.Manager.Commands;
public class ExecutionManager
{
    public enum ExecutionMode
    {
        Immediate,
        Batch
    }

    private enum BatchCommand
    {
        Start,
        Execute,
        Cancel
    }

    public ExecutionMode Mode = ExecutionMode.Immediate;
    public readonly List<ICommand> BatchQueue = [];

    private static BatchCommand ParseBatchCommand(string[] parameters)
    {
        if (parameters == null || parameters.Length == 0)
        {
            throw new InvalidInputException("Invalid batch command.");
        }

        if (Enum.TryParse(parameters[0], true, out BatchCommand batchCommand))
        {
            return batchCommand;
        }
        else
        {
            throw new InvalidInputException("Invalid batch command.");
        }
    }

    public void ExecuteBatchCommand(string[] parameters)
    {
        var batchCommand = ParseBatchCommand(parameters);

        switch (batchCommand)
        {
            case BatchCommand.Start:
                StartBatchMode();
                break;
            case BatchCommand.Execute:
                RunBatch();
                break;
            case BatchCommand.Cancel:
                CancelBatch();
                break;
            default:
                break;
        }
    }

    public void StartBatchMode()
    {
        if (Mode == ExecutionMode.Batch)
        {
            System.Console.WriteLine("Batch mode is already active.");
        }
        else
        {
            Mode = ExecutionMode.Batch;
            System.Console.WriteLine("Batch mode started.");
        }
    }

    public void RunBatch()
    {
        if (Mode == ExecutionMode.Immediate)
        {
            System.Console.WriteLine("Batch mode is not active.");
        }
        else
        {
            Mode = ExecutionMode.Immediate;

            foreach (var command in BatchQueue)
            {
                command.Execute();
            }

            BatchQueue.Clear();
            System.Console.WriteLine("Batch execution complete.");
        }
    }

    public void CancelBatch()
    {
        if (Mode == ExecutionMode.Immediate)
        {
            System.Console.WriteLine("Batch mode is not active.");
        }
        else
        {
            Mode = ExecutionMode.Immediate;
            BatchQueue.Clear();
            System.Console.WriteLine("Batch canceled.");
        }
    }

    public void AddToBatch(ICommand command)
    {
        BatchQueue.Add(command);
        System.Console.WriteLine("Command added to batch queue.");
    }
}
