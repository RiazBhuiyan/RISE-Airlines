﻿namespace Airlines.Console.Manager;
public class FileReader
{
    public static List<string[]> ReadFile(string filePath)
    {
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException($"The file '{filePath}' does not exist.");
        }

        var data = new List<string[]>();

        using (var reader = new StreamReader(filePath))
        {
            while (reader?.EndOfStream == false)
            {
                var line = reader.ReadLine();
                if (line != null)
                {
                    var values = line.Split(',');
                    data.Add(values);
                }
            }
        }

        return data;
    }
}
