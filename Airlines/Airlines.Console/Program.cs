﻿using Airlines.Console.Manager;
using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Reservation;
using System.Xml.Schema;
using System.Reflection;
using Airlines.Console.Manager.Commands;

namespace Airlines.Console;

public class Program
{
    private static void Main()
    {
        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var flightManager = new FlightManager();
        var aircraftManager = new AircraftManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        var airportsData = FileReader.ReadFile("../../../Data/airports.txt");
        if (airportsData.All(airport => airport.Length >= 4))
        {
            foreach (var airport in airportsData)
            {
                airportManager.AddAirport(new Airport(airport[0], airport[1], airport[2], airport[3]));
            }
        }
        else
        {
            System.Console.WriteLine("Invalid data format in airports file.");
        }

        var airlineData = FileReader.ReadFile("../../../Data/airlines.txt");
        if (airlineData.All(airline => airline.Length >= 1))
        {
            foreach (var airline in airlineData)
            {
                airlineManager.AddAirline(new Airline(airline[0]));
            }
        }
        else
        {
            System.Console.WriteLine("Invalid data format in airlines file.");
        }

        var flightData = FileReader.ReadFile("../../../Data/flights.txt");
        if (flightData.All(flight => flight.Length >= 3))
        {
            foreach (var flight in flightData)
            {
                flightManager.AddFlight(new Flight(flight[0], flight[1], flight[2], flight[3]));
            }
        }
        else
        {
            System.Console.WriteLine("Invalid data format in airlines file.");
        }

        var aircraftData = FileReader.ReadFile("../../../Data/aircrafts.txt");
        if (aircraftData.All(aircraft => aircraft.Length >= 4))
        {
            foreach (var aircraft in aircraftData)
            {
                aircraftManager.AddAircraft(aircraft[0], aircraft[1], aircraft[2], aircraft[3]);
            }
        }
        else
        {
            System.Console.WriteLine("Invalid data format in aircrafts file.");
        }

        var routeData = FileReader.ReadFile("../../../Data/routes.txt");
        flightManager.LoadRouteTreeData(routeData);


        foreach (var aircraft in aircraftManager.CargoAircraftList)
        {
            System.Console.WriteLine(aircraft.Model);
        }

        foreach (var aircraft in aircraftManager.PassengerAircraftList)
        {
            System.Console.WriteLine(aircraft.Model);
        }

        foreach (var aircraft in aircraftManager.PrivateAircraftList)
        {
            System.Console.WriteLine(aircraft.Model);
        }


        ConsoleManager.RunProgram(airportManager, airlineManager, flightManager, aircraftManager, reservationManager, executionManager);
    }
}
