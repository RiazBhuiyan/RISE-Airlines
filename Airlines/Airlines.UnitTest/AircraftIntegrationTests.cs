﻿using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Flight;

namespace Airlines.Tests
{
    public class AircraftIntegrationTests
    {
        [Fact]
        public void FlightData_IncludesAircraftModel()
        {
            // Arrange
            var flightManager = new FlightManager();
            var aircraftManager = new AircraftManager();
            var expectedModel = "Boeing 747";

            // Act
            aircraftManager.AddAircraft("Boeing 747", "140000", "854.5", "-");
            flightManager.AddFlight(new Flight("FL123", "Origin", "Destination", expectedModel));
            var flight = flightManager.GetFlightByIdentifier("FL123");

            // Assert
            Assert.NotNull(flight);
            Assert.Equal(expectedModel, flight.AircraftModel);
        }

        [Fact]
        public void AircraftData_LoadedCorrectlyFromFile()
        {
            // Arrange
            var aircraftManager = new AircraftManager();

            // Act
            aircraftManager.AddAircraft("Boeing 747", "140000", "854.5", "-");
            aircraftManager.AddAircraft("Airbus A320", "20000", "37.4", "150");
            aircraftManager.AddAircraft("Gulfstream G650", "-", "-", "18");

            // Assert
            Assert.NotEmpty(aircraftManager.CargoAircraftList);
            Assert.NotEmpty(aircraftManager.PassengerAircraftList);
            Assert.NotEmpty(aircraftManager.PrivateAircraftList);
        }
    }
}
