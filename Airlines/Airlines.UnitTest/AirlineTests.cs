﻿using Airlines.Business.Data.Airline;

namespace Airlines.UnitTest
{
    public class AirlineTest
    {
        [Fact]
        public void Airline_SetValidName_ShouldSet()
        {
            // Arrange
            var validName = "ABC";
            var airline = new Airline(validName);

            // Act
            var result = airline.Name;

            // Assert
            Assert.Equal(validName, result);
        }

        [Theory]
        [InlineData("ABC")]
        [InlineData("XYZ")]
        public void IsValidAirline_ShouldReturnTrueForValidNames(string validName)
        {
            // Act
            var result = Airline.IsValidAirline(validName);

            // Assert
            Assert.True(result);
        }

        [Theory]
        [InlineData("ABCDEF")]
        [InlineData("LongerNameThanMaxLength")]
        public void IsValidAirline_ShouldReturnFalseForInvalidNames(string invalidName)
        {
            // Act
            var result = Airline.IsValidAirline(invalidName);

            // Assert
            Assert.False(result);
        }
    }
}
