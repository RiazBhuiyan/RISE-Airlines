﻿using Xunit;
using Airlines.Business.Data.Airport;
using Airlines.Console.Manager;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using Airlines.Console.Manager.Commands;

namespace Airlines.UnitTest;

public class AirportManagerTests
{
    [Fact]
    public void ListAirports_ByCountry_PrintsAirportsInCountry()
    {
        // Arrange
        var airportManager = new AirportManager();
        var airport = new Airport("TST", "Test", "Test City", "Test Country");
        airportManager.AddAirport(airport);

        // Act & Assert
        using var sw = new StringWriter();
        System.Console.SetOut(sw);
        airportManager.ListAirports("Test Country", "country");
        var output = sw.ToString().Trim();

        Assert.Contains("Airports in Test Country:", output);
        Assert.Contains($"Name: {airport.Name}, Identifier: {airport.Identifier}", output);
    }

    [Fact]
    public void ListAirports_InvalidFrom_PrintsErrorMessage()
    {
        // Arrange
        var airportManager = new AirportManager();

        // Act & Assert
        using var sw = new StringWriter();
        System.Console.SetOut(sw);
        airportManager.ListAirports("Test", "invalid");
        var output = sw.ToString().Trim();

        Assert.Contains("Invalid input for 'from'. Please specify 'city' or 'country'.", output);

    }

    [Fact]
    public void HandleSortCommand_SortsAirportsAscending_Successfully()
    {
        // Arrange
        var airports = new List<string> { "Z", "A", "C", "B" };
        var airlines = new List<string> { "Airline1", "Airline2", "Airline3" };
        var flights = new List<string> { "Flight1", "Flight2", "Flight3" };
        var parameters = new string[] { "Airports", "Ascending" };

        // Act
        string output;
        using (var sw = new StringWriter())
        {
            System.Console.SetOut(sw);
            var sortCommand = new SortCommand(parameters, airports, airlines, flights);
            sortCommand.Execute();
            output = sw.ToString().Trim();
        }

        // Assert
        Assert.Equal("A, B, C, Z", string.Join(", ", airports));
    }
}
