﻿using Airlines.Business.Data.Airport;
using Airlines.Business.Exceptions;

namespace Airlines.UnitTest
{
    public class AirportTests
    {
        [Fact]
        public void Airport_SetValidValues_ShouldSet()
        {
            // Arrange
            var identifier = "ABC";
            var name = "AirportName";
            var city = "CityName";
            var country = "CountryName";

            // Act
            var airport = new Airport(identifier, name, city, country);

            // Assert
            Assert.Equal(identifier, airport.Identifier);
            Assert.Equal(name, airport.Name);
            Assert.Equal(city, airport.City);
            Assert.Equal(country, airport.Country);
        }

        [Fact]
        public void Airport_SetInvalidName_ShouldThrowInvalidAirportException()
        {
            // Arrange
            var invalidName = "123"; // Invalid characters

            // Act & Assert
            _ = Assert.Throws<InvalidAirportException>(() => new Airport("ABC", invalidName, "City", "Country"));
        }

        [Fact]
        public void Airport_SetInvalidCity_ShouldThrowInvalidAirportException()
        {
            // Arrange
            var invalidCity = "123"; // Invalid characters

            // Act & Assert
            _ = Assert.Throws<InvalidAirportException>(() => new Airport("ABC", "Name", invalidCity, "Country"));
        }

        [Fact]
        public void Airport_SetInvalidCountry_ShouldThrowInvalidAirportException()
        {
            // Arrange
            var invalidCountry = "123"; // Invalid characters

            // Act & Assert
            _ = Assert.Throws<InvalidAirportException>(() => new Airport("ABC", "Name", "City", invalidCountry));
        }
    }
}
