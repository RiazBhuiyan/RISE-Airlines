﻿using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Airline;
using Airlines.Business.Data.Airport;
using Airlines.Business.Data.Flight;
using Airlines.Business.Data.Reservation;
using Airlines.Console.Manager;
using Airlines.Console.Manager.Commands;
namespace Airlines.UnitTest;
public class ConsoleManagerTests
{
    [Fact]
    public void AirlineExists_ShouldReturnFalse()
    {
        // Arrange
        var airlineManager = new AirlineManager();
        var airline = new Airline("BGAir");
        var stringWriter = new StringWriter();
        System.Console.SetOut(stringWriter);

        // Act
        airlineManager.AddAirline(airline);
        var result = airlineManager.AirlineExists("BAir"); // Corrected the airline name

        // Assert
        Assert.False(result);
        Assert.Contains($"Airline {airline.Name} added successfully.", stringWriter.ToString(), StringComparison.Ordinal);
    }

    [Fact]
    public void ImmediateCommandExecution_ExecutesCommandsImmediately()
    {
        // Arrange
        var input = "exist BGAir\nexit";
        var reader = new StringReader(input);
        System.Console.SetIn(reader);
        var stringWriter = new StringWriter();
        System.Console.SetOut(stringWriter);

        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        airlineManager.AddAirline(new Airline("BGAir"));

        // Act
        ConsoleManager.RunProgram(airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager,
                        executionManager
                        );

        // Assert
        Assert.Contains("True", stringWriter.ToString());
    }

    [Fact]
    public void BatchCommandExecution_StartsBatchMode()
    {
        // Arrange
        var input = "batch start\nexit";
        var reader = new StringReader(input);
        System.Console.SetIn(reader);
        var stringWriter = new StringWriter();
        System.Console.SetOut(stringWriter);

        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        // Act
        ConsoleManager.RunProgram(airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager,
                        executionManager
                        );

        // Assert
        Assert.Contains("Batch mode started.", stringWriter.ToString());
    }

    [Fact]
    public void BatchCommandExecution_ExitsBatchMode()
    {
        // Arrange
        var input = "batch start\nbatch cancel\nexit";
        var reader = new StringReader(input);
        System.Console.SetIn(reader);
        var stringWriter = new StringWriter();
        System.Console.SetOut(stringWriter);

        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        // Act
        ConsoleManager.RunProgram(airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager,
                        executionManager
                        );

        // Assert
        Assert.Contains("Batch canceled.", stringWriter.ToString());
    }

    [Fact]
    public void BatchCommandExecution_AddsCommandsToQueue()
    {
        // Arrange
        var input = "batch start\nexist BGAir\nexit";
        var reader = new StringReader(input);
        System.Console.SetIn(reader);

        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        airlineManager.AddAirline(new Airline("BGAir"));

        // Act
        ConsoleManager.RunProgram(airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager,
                        executionManager
                        );

        // Assert
        _ = Assert.Single(executionManager.BatchQueue);
        _ = Assert.IsAssignableFrom<ICommand>(executionManager.BatchQueue[0]);
    }

    [Fact]
    public void BatchCommandExecution_ExecutesBatchCommands()
    {
        // Arrange
        var input = "batch start\nexist BGAir\nbatch execute\nexit";
        var reader = new StringReader(input);
        System.Console.SetIn(reader);
        var stringWriter = new StringWriter();
        System.Console.SetOut(stringWriter);

        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var aircraftManager = new AircraftManager();
        var flightManager = new FlightManager();
        var reservationManager = new ReservationManager();
        var executionManager = new ExecutionManager();

        airlineManager.AddAirline(new Airline("BGAir"));

        // Act
        ConsoleManager.RunProgram(airportManager,
                        airlineManager,
                        flightManager,
                        aircraftManager,
                        reservationManager,
                        executionManager
                        );

        // Assert
        Assert.Contains("True", stringWriter.ToString());
        Assert.Contains("Batch execution complete.", stringWriter.ToString());
        Assert.Empty(executionManager.BatchQueue);
    }

    [Theory]
    [InlineData("JFK", "Airport found: JFK.")]
    [InlineData("BGAir", "Airline found: BGAir.")]
    [InlineData("Flight1", "Flight found: Flight1.")]
    public void SearchAllLists_FindsSearchTerm(string searchTerm, string expectedResult)
    {
        // Arrange
        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var flightManager = new FlightManager();
        var airport = new Airport("JFK", "JFK International Airport", "NY", "USA");
        airportManager.AddAirport(airport);
        var airline = new Airline("BGAir");
        airlineManager.AddAirline(airline);
        var flight = new Flight("Flight1", "JFK", "LAX", "Airbus A320");
        flightManager.AddFlight(flight);
        var consoleOutput = new StringWriter();

        // Act
        System.Console.SetOut(consoleOutput);
        SearchCommand.SearchAllLists(
            airportManager.AirportList,
            [.. airlineManager.AirlineDictionary.Values],
            flightManager.FlightList,
            searchTerm
            );
        var output = consoleOutput.ToString();

        // Assert
        Assert.Contains(expectedResult, output);
    }

    [Theory]
    [InlineData("SOF", "SOF not found.")]
    [InlineData("SFAir", "SFAir not found.")]
    [InlineData("Flight123", "Flight123 not found.")]
    public void SearchAllLists_DoesNotFindSearchTerm(string searchTerm, string expectedResult)
    {
        // Arrange
        var airportManager = new AirportManager();
        var airlineManager = new AirlineManager();
        var flightManager = new FlightManager();
        var airport = new Airport("JFK", "JFK International Airport", "NY", "USA");
        airportManager.AddAirport(airport);
        var airline = new Airline("BGAir");
        airlineManager.AddAirline(airline);
        var flight = new Flight("Flight1", "JFK", "LAX", "Airbus A320");
        flightManager.AddFlight(flight);
        var consoleOutput = new StringWriter();
        System.Console.SetOut(consoleOutput);

        // Act
        SearchCommand.SearchAllLists(
            airportManager.AirportList,
            [.. airlineManager.AirlineDictionary.Values],
            flightManager.FlightList,
            searchTerm
            );
        var output = consoleOutput.ToString();

        // Assert
        Assert.Contains(expectedResult, output);
    }
}


