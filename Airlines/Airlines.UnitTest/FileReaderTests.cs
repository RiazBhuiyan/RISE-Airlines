﻿using Airlines.Console.Manager;

#pragma warning disable IDE0055
namespace Airlines.UnitTest;
public class FileReaderTest
{
    [Fact]
    public void FileReader_ShouldRead()
    {
        // Arrange && Act
        var result = FileReader.ReadFile("../../../TestData/airports.txt");

        // Assert
        Assert.True(result.Count > 1);
    }

    [Fact]
    public void FileReader_ShouldThrowFileNotFoundException()
        // Arrange && Act && Assert
        => Assert.Throws<FileNotFoundException>(() => FileReader.ReadFile("nonexistentfile.txt"));
}

