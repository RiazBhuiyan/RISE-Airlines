﻿using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;

#pragma warning disable IDE0055
namespace Airlines.UnitTest
{
    public class FlightTests
    {
        [Fact]
        public void Flight_SetValidValues_ShouldSet()
        {
            // Arrange
            var identifier = "FL123";
            var departureAirport = "ATL";
            var arrivalAirport = "JFK";
            var aircraftModel = "Boeing 737";

            // Act
            var flight = new Flight(identifier, departureAirport, arrivalAirport, aircraftModel);

            // Assert
            Assert.Equal(identifier, flight.Identifier);
            Assert.Equal(departureAirport, flight.DepartureAirport);
            Assert.Equal(arrivalAirport, flight.ArrivalAirport);
            Assert.Equal(aircraftModel, flight.AircraftModel);
        }

        [Fact]
        public void Flight_SetInvalidIdentifier_ShouldThrowInvalidFlightCodeException()
        {
            // Arrange
            var invalidIdentifier = "FL-123"; // Contains non-alphanumeric characters

            // Act & Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => new Flight(invalidIdentifier, "ATL", "JFK", "Boeing 737"));
        }

        [Fact]
        public void Flight_SetEmptyAircraftModel_ShouldThrowInvalidAircraftParameterException()
            // Act & Assert
            => Assert.Throws<InvalidAircraftParameterException>(() => new Flight("FL123", "ATL", "JFK", ""));
    }
}
