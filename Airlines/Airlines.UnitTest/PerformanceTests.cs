﻿using System.Diagnostics;
using Xunit.Abstractions;
using Airlines.Business.Utilities;

namespace Airlines.UnitTest
{
    public class PerformanceTests(ITestOutputHelper output)
    {
        private readonly ITestOutputHelper _output = output;

        [Fact]
        public void BubbleSort_SelectionSort_Comparison_SelectionQuicker_ReturnsTrue()
        {
            var dataForSorting = GenerateRandomData(1000);
            var timeLimit = TimeSpan.FromSeconds(1);

            var stopwatchBubble = Stopwatch.StartNew();
            var iterationsBubble = 0;
            while (stopwatchBubble.Elapsed < timeLimit)
            {
                ListExtensions.BubbleSort(dataForSorting);
                iterationsBubble++;
            }
            stopwatchBubble.Stop();

            var stopwatchSelection = Stopwatch.StartNew();
            var iterationsSelection = 0;
            while (stopwatchSelection.Elapsed < timeLimit)
            {
                ListExtensions.SelectionSort(dataForSorting);
                iterationsSelection++;
            }
            stopwatchSelection.Stop();
            _output.WriteLine($"Bubble Sort: Completed {iterationsBubble} iterations in {timeLimit.TotalSeconds} seconds");
            _output.WriteLine($"Selection Sort: Completed {iterationsSelection} iterations in {timeLimit.TotalSeconds} seconds");
        }

        [Fact]
        public void BubbleSort_PerformanceTest_ReturnsIterationsAndTotalSeconds()
        {
            var dataForSorting = GenerateRandomData(1000);
            var timeLimit = TimeSpan.FromSeconds(1);

            var stopwatch = Stopwatch.StartNew();
            var iterations = 0;
            while (stopwatch.Elapsed < timeLimit)
            {
                ListExtensions.BubbleSort(dataForSorting);
                iterations++;
            }
            stopwatch.Stop();

            _output.WriteLine($"Bubble Sort: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
        }

        [Fact]
        public void SelectionSort_PerformanceTest_ReturnsIterationsAndTotalSeconds()
        {
            var dataForSorting = GenerateRandomData(1000);
            var timeLimit = TimeSpan.FromSeconds(1);

            var stopwatch = Stopwatch.StartNew();
            var iterations = 0;
            while (stopwatch.Elapsed < timeLimit)
            {
                ListExtensions.SelectionSort(dataForSorting);
                iterations++;
            }
            stopwatch.Stop();
            _output.WriteLine($"Selection Sort: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
        }

        [Fact]
        public void LinearSearch_PerformanceTest_ReturnsIterationsAndTotalSeconds()
        {
            var dataForSearching = GenerateRandomData(1000);
            var timeLimit = TimeSpan.FromSeconds(1);

            var stopwatch = Stopwatch.StartNew();
            var iterations = 0;
            while (stopwatch.Elapsed < timeLimit)
            {
                _ = ListExtensions.ValidateUniqueness(dataForSearching);
                iterations++;
            }
            stopwatch.Stop();
            _output.WriteLine($"Linear Search: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
        }

        [Fact]
        public void BinarySearch_PerformanceTest_ReturnsIterationsAndTotalSeconds()
        {
            var dataForSearching = GenerateAndSortRandomData(1000);
            var timeLimit = TimeSpan.FromSeconds(1);

            var stopwatch = Stopwatch.StartNew();
            var iterations = 0;
            while (stopwatch.Elapsed < timeLimit)
            {
                _ = ListExtensions.BinarySearch(dataForSearching, "AAA");
                iterations++;
            }
            stopwatch.Stop();
            _output.WriteLine($"Binary Search: Completed {iterations} iterations in {timeLimit.TotalSeconds} seconds");
        }

        [Fact]
        public void BinarySearch_LinearSearch_ComparePerformance_ReturnsTrue()
        {
            var dataForSearching = GenerateAndSortRandomData(1000);
            var target = dataForSearching[700];

            var stopwatchBinary = Stopwatch.StartNew();
            _ = ListExtensions.BinarySearch(dataForSearching, target);
            stopwatchBinary.Stop();

            var stopwatchLinear = Stopwatch.StartNew();
            _ = ListExtensions.ValidateUniqueness(dataForSearching);
            stopwatchLinear.Stop();

            Assert.True(stopwatchBinary.Elapsed < stopwatchLinear.Elapsed);
            _output.WriteLine($"Binary Search: Completed in {stopwatchBinary.Elapsed}");
            _output.WriteLine($"Linear Search: Completed in {stopwatchLinear.Elapsed}");
        }


        public static List<string> GenerateRandomData(int size)
        {
            var list = new List<string>();
            var random = new Random();

            // Define a pool of characters
            var pool = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()";

            for (var i = 0; i < size; i++)
            {
                var randomChars = new char[10]; // Adjust the length of the random string as needed

                // Populate the randomChars array with random characters from the pool
                for (var j = 0; j < randomChars.Length; j++)
                {
                    randomChars[j] = pool[random.Next(pool.Length)];
                }

                // Convert the char array to a string and add it to the list
                list.Add(new string(randomChars));
            }

            return list;
        }

        public static List<string> GenerateAndSortRandomData(int size)
        {
            var list = GenerateRandomData(size);
            list.Sort();
            return list;
        }
    }
}
