﻿using Airlines.Business.Data.Reservation;
using Airlines.Business.Data.Aircraft;
using Airlines.Business.Data.Flight;

namespace Airlines.Tests
{
    public class ReservationTests
    {
        [Fact]
        public void CargoAircraft_ShouldCreateInstance()
        {
            // Arrange
            var model = "Boeing 747";
            var cargoWeight = 140000;
            var cargoVolume = 854.5;

            // Act
            var aircraft = new CargoAircraft(model, cargoWeight, cargoVolume);

            // Assert
            Assert.Equal(model, aircraft.Model);
            Assert.Equal(cargoWeight, aircraft.CargoWeight);
            Assert.Equal(cargoVolume, aircraft.CargoVolume);
        }

        [Fact]
        public void PassengerAircraft_ShouldCreateInstance()
        {
            // Arrange
            var model = "Airbus A320";
            var cargoWeight = 20000;
            var cargoVolume = 37.4;
            var seats = 150;

            // Act
            var aircraft = new PassengerAircraft(model, cargoWeight, cargoVolume, seats);

            // Assert
            Assert.Equal(model, aircraft.Model);
            Assert.Equal(cargoWeight, aircraft.CargoWeight);
            Assert.Equal(cargoVolume, aircraft.CargoVolume);
            Assert.Equal(seats, aircraft.Seats);
        }

        [Fact]
        public void PrivateAircraft_ShouldCreateInstance()
        {
            // Arrange
            var model = "Gulfstream G650";
            var seats = 18;

            // Act
            var aircraft = new PrivateAircraft(model, seats);

            // Assert
            Assert.Equal(model, aircraft.Model);
            Assert.Equal(seats, aircraft.Seats);
        }

        [Fact]
        public void AircraftManager_ShouldAddPassengerAircraft()
        {
            // Arrange
            var aircraftManager = new AircraftManager();
            var model = "Airbus A320";
            var cargoWeight = 20000;
            var cargoVolume = 37.4;
            var seats = 150;

            // Act
            aircraftManager.AddAircraft(model, cargoWeight.ToString(), cargoVolume.ToString(), seats.ToString());

            // Assert
            Assert.Equal(model, aircraftManager.PassengerAircraftList[0].Model);
            Assert.Equal(cargoWeight, aircraftManager.PassengerAircraftList[0].CargoWeight);
            Assert.Equal(cargoVolume, aircraftManager.PassengerAircraftList[0].CargoVolume);
            Assert.Equal(seats, aircraftManager.PassengerAircraftList[0].Seats);
        }

        [Fact]
        public void AircraftManager_ShouldAddCargoAircraft()
        {
            // Arrange
            var aircraftManager = new AircraftManager();
            var model = "Boeing 747";
            var cargoWeight = 140000;
            var cargoVolume = 854.5;

            // Act
            aircraftManager.AddAircraft(model, cargoWeight.ToString(), cargoVolume.ToString(), "-");

            // Assert
            Assert.Equal(model, aircraftManager.CargoAircraftList[0].Model);
            Assert.Equal(cargoWeight, aircraftManager.CargoAircraftList[0].CargoWeight);
            Assert.Equal(cargoVolume, aircraftManager.CargoAircraftList[0].CargoVolume);
        }

        [Fact]
        public void AircraftManager_ShouldAddPrivateAircraft()
        {
            // Arrange
            var aircraftManager = new AircraftManager();
            var model = "Gulfstream G650";
            var seats = 18;

            // Act
            aircraftManager.AddAircraft(model, "-", "-", seats.ToString());

            // Assert
            Assert.Equal(model, aircraftManager.PrivateAircraftList[0].Model);
            Assert.Equal(seats, aircraftManager.PrivateAircraftList[0].Seats);
        }

        [Fact]
        public void CargoReservation_ShouldCreateInstance()
        {
            // Arrange
            var flightIdentifier = "FL123";
            var cargoWeight = 123;
            var cargoVolume = 22.5;

            // Act
            var reservation = new CargoReservation(flightIdentifier, cargoWeight, cargoVolume);

            // Assert
            Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
            Assert.Equal(cargoWeight, reservation.CargoWeight);
            Assert.Equal(cargoVolume, reservation.CargoVolume);
        }

        [Fact]
        public void TicketReservation_ShouldCreateInstance()
        {
            // Arrange
            var flightIdentifier = "FL123";
            var seats = 2;
            var smallBaggageCount = 2;
            var largeBaggageCount = 3;

            // Act
            var reservation = new TicketReservation(flightIdentifier, seats, smallBaggageCount, largeBaggageCount);

            // Assert
            Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
            Assert.Equal(seats, reservation.Seats);
            Assert.Equal(smallBaggageCount, reservation.SmallBaggageCount);
            Assert.Equal(largeBaggageCount, reservation.LargeBaggageCount);
        }

        [Fact]
        public void ReserveCargo_SubmitsReservation()
        {
            // Arrange
            var reservationManager = new ReservationManager();
            var flightManager = new FlightManager();
            var aircraftManager = new AircraftManager();
            var flightIdentifier = "FL123";
            var airCraftCargoWeight = "20000";
            var aircraftCargoVolume = "37.4";
            var aircraftSeats = "-";

            var reservationCargoWeight = 240;
            var reservationCargoVolume = 1.5;

            flightManager.AddFlight(new Flight(flightIdentifier, "JFK", "LAX", "Airbus A320"));
            aircraftManager.AddAircraft("Airbus A320", airCraftCargoWeight, aircraftCargoVolume, aircraftSeats);

            // Act
            reservationManager.ReserveCargo(flightIdentifier, reservationCargoWeight, reservationCargoVolume, flightManager, aircraftManager);
            var reservation = reservationManager.CargoReservationList.ToList()[0];

            // Assert
            _ = Assert.Single(reservationManager.CargoReservationList);
            Assert.Equal(flightIdentifier, reservation.FlightIdentifier);
            Assert.Equal(reservationCargoWeight, reservation.CargoWeight);
            Assert.Equal(reservationCargoVolume, reservation.CargoVolume);
        }
    }
}
