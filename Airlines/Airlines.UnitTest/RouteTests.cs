﻿using Airlines.Business.Data.Flight;
using Airlines.Business.Exceptions;
using Airlines.Console.Manager;
using Xunit;

namespace Airlines.UnitTest
{
    public class RouteTest
    {
        [Fact]
        public void Route_ShouldBeCreatedSuccessfully()
        {
            // Arrange
            var flightManager = new FlightManager();
            var route = new Route();

            // Act
            flightManager.Route = route;

            // Assert
            Assert.Equal(route, flightManager.Route);
        }

        [Fact]
        public void Route_ShouldAddRoute()
        {
            // Arrange
            var route = new Route();
            var flightManager = new FlightManager();
            var flight = new Flight("FL123", "JFK", "LAX", "Cargo");
            flightManager.AddFlight(flight);

            // Act
            route.AddFlight("FL123", flightManager);

            // Assert
            Assert.Contains(flight, route.RouteList);
        }

        [Fact]
        public void Route_ShouldRemoveRoute()
        {
            // Arrange
            var route = new Route();
            var flightManager = new FlightManager();
            var flight = new Flight("FL123", "JFK", "LAX", "Cargo");
            flightManager.AddFlight(flight);
            route.AddFlight("FL123", flightManager);

            // Act
            route.RemoveLastFlight();

            // Assert
            Assert.Empty(route.RouteList);
        }

        [Fact]
        public void RouteAddFlight_InvalidFlight_Throws()
        {
            // Arrange
            var route = new Route();
            var flightManager = new FlightManager();

            // Act & Assert
            _ = Assert.Throws<InvalidFlightCodeException>(() => route.AddFlight("Flight123", flightManager));
        }

        [Fact]
        public void RouteAddFlight_IllogicalConnectionFlight_Throws()
        {
            // Arrange
            var route = new Route();
            var flightManager = new FlightManager();
            var flight = new Flight("FL123", "JFK", "LAX", "Airbus A320");
            var flightTwo = new Flight("FL234", "RCA", "OAE", "Airbus A320");
            flightManager.AddFlight(flight);
            flightManager.AddFlight(flightTwo);
            route.AddFlight("FL123", flightManager);

            // Act & Assert
            _ = Assert.Throws<InvalidRouteException>(() => route.AddFlight("FL234", flightManager));
        }

        [Fact]
        public void RouteRemoveLastFlight_NoFlights_Throws()
        {
            // Arrange
            var route = new Route();

            // Act & Assert
            _ = Assert.Throws<InvalidRouteException>(route.RemoveLastFlight);
        }

        [Theory]
        [InlineData("ATL")]
        [InlineData("JFK")]
        [InlineData("ORD")]
        public void FindRoute_ShouldReturnTreeNode(string route)
        {
            // Arrange
            var routeData = FileReader.ReadFile("../../../TestData/routes.txt");
            var flightData = FileReader.ReadFile("../../../TestData/flights.txt")
                .Select(flight => new Flight(flight[0], flight[1], flight[2], flight[3]));
            var flightManager = new FlightManager();
            foreach (var flight in flightData)
            {
                flightManager.AddFlight(flight);
            }
            flightManager.LoadRouteTreeData(routeData);

            // Act
            var routeNode = flightManager.RouteTree.FindRoute(route);

            // Assert
            Assert.Equal(route, routeNode!.Value.ArrivalAirport);
        }

        [Fact]
        public void GetFlightRoute_ShouldReturnCorrectRoute()
        {
            // Arrange
            var routeData = FileReader.ReadFile("../../../TestData/routes.txt");
            var flightData = FileReader.ReadFile("../../../TestData/flights.txt")
                .Select(flight => new Flight(flight[0], flight[1], flight[2], flight[3]));
            var flightManager = new FlightManager();
            foreach (var flight in flightData)
            {
                flightManager.AddFlight(flight);
            }
            flightManager.LoadRouteTreeData(routeData);
            var searchRoute = "ATL";

            // Act
            var foundRoute = flightManager.RouteTree.GetFlightRoute(searchRoute);

            // Assert
            Assert.Equal("DFW->JFK->LAX->ORD->ATL", foundRoute);
        }


        [Theory]
        [InlineData("DFW")]
        [InlineData("SOF")]
        public void FindRoute_ShouldReturnNull(string route)
        {
            // Arrange
            var routeData = FileReader.ReadFile("../../../TestData/routes.txt");
            var flightData = FileReader.ReadFile("../../../TestData/flights.txt")
                .Select(flight => new Flight(flight[0], flight[1], flight[2], flight[3]));
            var flightManager = new FlightManager();
            foreach (var flight in flightData)
            {
                flightManager.AddFlight(flight);
            }
            flightManager.LoadRouteTreeData(routeData);
            var foundRoute = flightManager.RouteTree.FindRoute(route);

            // Act & Assert
            Assert.Null(foundRoute);
        }

        [Theory]
        [InlineData("DFW")]
        [InlineData("SOF")]
        public void GetFlightRoute_ShouldReturnNull(string route)
        {
            // Arrange
            var routeData = FileReader.ReadFile("../../../TestData/routes.txt");
            var flightData = FileReader.ReadFile("../../../TestData/flights.txt")
                .Select(flight => new Flight(flight[0], flight[1], flight[2], flight[3]));
            var flightManager = new FlightManager();
            foreach (var flight in flightData)
            {
                flightManager.AddFlight(flight);
            }
            flightManager.LoadRouteTreeData(routeData);
            var foundRoute = flightManager.RouteTree.GetFlightRoute(route);

            // Act & Assert
            Assert.Null(foundRoute);
        }
    }
}
