﻿using Airlines.Business.Utilities;

namespace Airlines.UnitTest
{
    public class SearchingTests
    {
        // Linear Search Unit Tests
        [Fact]
        public void LinearSearch_ReturnsTrueForUniqueData()
        {
            // Arrange
            List<string> data = ["a", "b", "c"];

            // Act
            var result = ListExtensions.ValidateUniqueness(data);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void LinearSearch_ReturnsFalseForNonUniqueData()
        {
            // Arrange
            List<string> data = ["a", "b", "c", "b"];

            // Act
            var result = ListExtensions.ValidateUniqueness(data);

            // Assert
            Assert.False(result);
        }

        // Binary Search Unit Tests
        [Fact]
        public void BinarySearch_FindsElementInSortedStringArray()
        {
            // Arrange
            List<string> data = ["apple", "banana", "grape", "orange", "strawberry"];

            // Act
            var result = ListExtensions.BinarySearch(data, "banana");

            // Assert
            Assert.True(result);
        }

        [Fact]
        public void BinarySearch_DoesNotFindElementInSortedStringArray()
        {
            // Arrange
            List<string> data = ["apple", "banana", "grape", "orange", "strawberry"];

            // Act
            var result = ListExtensions.BinarySearch(data, "kiwi");

            // Assert
            Assert.False(result);
        }
    }
}
