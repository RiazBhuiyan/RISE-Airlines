﻿using Airlines.Business.Utilities;

namespace Airlines.UnitTest
{
    public class SortingTests
    {
        // Bubble Sort Unit Tests
        [Fact]
        public void BubbleSort_SortsStringArrayInAscendingOrder()
        {
            // Arrange
            List<string> data = ["banana", "apple", "grape", "orange"];
            List<string> expected = ["apple", "banana", "grape", "orange"];

            // Act
            ListExtensions.BubbleSort(data);

            // Assert
            Assert.Equal(expected, data);
        }

        // Selection Sort Unit Tests
        [Fact]
        public void SelectionSort_SortsStringArrayInAscendingOrder()
        {
            // Arrange
            List<string> data = ["banana", "apple", "grape", "orange"];
            List<string> expected = ["apple", "banana", "grape", "orange"];

            // Act
            ListExtensions.SelectionSort(data);

            // Assert
            Assert.Equal(expected, data);
        }
    }
}
