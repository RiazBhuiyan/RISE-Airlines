using Airlines.Business.Utilities;
using Airlines.Business.Data.Flight;

namespace Airlines.UnitTest;

public class ValidationTests
{
    //Validation tests for Airports
    [Theory]
    [InlineData("JFK", true)] // Valid airport code
    [InlineData("LAX", true)] // Valid airport code
    [InlineData("ATL", true)] // Valid airport code
    [InlineData("ABCD", false)] // Invalid airport code (not exactly 3 characters)
    [InlineData("123", false)] // Invalid airport code (contains digits)
    [InlineData("12345", false)] // Invalid airport code (more than 3 characters)
    [InlineData("abcde", false)] // Invalid airport code (more than 3 characters)
    [InlineData("A B", false)] // Invalid airport code (contains space)
    [InlineData("JFK!", false)] // Invalid airport code (contains exclamation mark)
    [InlineData("LA-X", false)] // Invalid airport code (contains hyphen)
    public void AirportValidationTest(string input, bool expectedResult)
    {
        // Act
        var result = DataValidator.ValidateInput(input, "Airports");

        // Assert
        Assert.Equal(expectedResult, result);
    }


    //Validation tests for Airlines
    [Theory]
    [InlineData("Delta", true)] // Valid airline name (less than 6 characters)
    [InlineData("United", false)] // Invalid airline name (6 characters)
    [InlineData("AA", true)] // Valid airline name (less than 6 characters)
    [InlineData("JetBlue", false)] // Invalid airline name (more than 6 characters)
    public void AirlineValidationTest(string input, bool expectedResult)
    {
        // Act
        var result = DataValidator.ValidateInput(input, "Airlines");

        // Assert
        Assert.Equal(expectedResult, result);
    }


    //Validation tests for Flights
    [Theory]
    [InlineData("DL123", true)] // Valid flight number
    [InlineData("UA234", true)] // Valid flight number
    [InlineData("DL 123", false)] // Invalid flight number (contains space)
    [InlineData("UA-234", false)] // Invalid flight number (contains hyphen)
    [InlineData("DL123A!", false)] // Invalid flight number (contains exclamation mark)
    public void FlightValidationTest(string input, bool expectedResult)
    {
        // Act
        var result = DataValidator.ValidateInput(input, "Flights");

        // Assert
        Assert.Equal(expectedResult, result);
    }


    // IsAlphabetic Method Tests
    [Theory]
    [InlineData("abc", true)] // All alphabetic characters
    [InlineData("ABC", true)] // All alphabetic characters
    [InlineData("AbC", true)] // All alphabetic characters
    [InlineData("123", false)] // Contains digits
    [InlineData("abc123", false)] // Contains digits
    [InlineData("", true)] // Empty string (considered alphabetic)
    public void IsAlphabetic_ReturnsCorrectResult(string input, bool expected)
    {
        // Act
        var result = DataValidator.IsAlphabetic(input);

        // Assert
        Assert.Equal(expected, result);
    }


    // IsAlphanumeric Method Tests
    [Theory]
    [InlineData("abc123", true)] // Alphanumeric string
    [InlineData("ABC123", true)] // Alphanumeric string
    [InlineData("123", true)] // Numeric string
    [InlineData("abc", true)] // Alphabetic string
    [InlineData("!@#", false)] // Non-alphanumeric string
    public void IsAlphanumeric_ReturnsCorrectResult(string input, bool expected)
    {
        // Act
        var result = DataValidator.IsAlphanumeric(input);

        // Assert
        Assert.Equal(expected, result);
    }


    // SwapItemsInArray Method Test
    [Fact]
    public void SwapItemsInArray_SwapsItemsCorrectly()
    {
        // Arrange
        List<string> data = ["a", "b", "c"];
        List<string> expected = ["c", "b", "a"];

        // Act
        ListExtensions.SwapItems(data, 0, 2);

        // Assert
        Assert.Equal(expected, data);
    }

    [Fact]
    public void ListToString_EmptyList_ReturnsEmptyString()
    {
        // Arrange
        var list = new List<string>();

        // Act
        var result = ListExtensions.ListToString(list);

        // Assert
        Assert.Equal("", result);
    }

    [Fact]
    public void ListToString_SingleItemList_ReturnsSingleItem()
    {
        // Arrange
        var list = new List<string> { "Item1" };

        // Act
        var result = ListExtensions.ListToString(list);

        // Assert
        Assert.Equal("Item1", result);
    }

    [Fact]
    public void ListToString_MultipleItemList_ReturnsCommaSeparatedString()
    {
        // Arrange
        var list = new List<string> { "Item1", "Item2", "Item3" };

        // Act
        var result = ListExtensions.ListToString(list);

        // Assert
        Assert.Equal("Item1, Item2, Item3", result);
    }
}